from django.conf import settings
from django.conf.urls import patterns, include, url, static
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^', include('base.urls')),

    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)