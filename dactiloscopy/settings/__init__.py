import os

ENV = os.environ.get('ENV', 'PROD').upper()
DEV = ENV == 'DEV'
STAGING = ENV == 'STAGING'

if DEV:
    from dactiloscopy.settings.dev import *
elif STAGING:
    from dactiloscopy.settings.staging import *