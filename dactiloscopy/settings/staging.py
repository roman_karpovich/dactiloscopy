from dactiloscopy.settings.base import *
from dactiloscopy.settings import deploy_staging as deploy
from dactiloscopy.settings.deploy_staging import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME

STAGING = True
DEV = False
PROD = False
ENV = 'STAGING'

DEBUG = TEMPLATE_DEBUG = False

ADMINS = (
    ('Roman Karpovich', 'fpm.th13f@gmail.com'),
)

CURRENT_HOST = 'dactiloscopy.razortheory.com'
BASE_URL = "http://" + CURRENT_HOST
ALLOWED_HOSTS = [deploy.HOST, '.' + CURRENT_HOST]

DATABASES['default']['HOST'] = deploy.DB_HOST
DATABASES['default']['PORT'] = '5432'

if USE_CLOUDFRONT:
    AWS_S3_CUSTOM_DOMAIN = 'static.%s' % CURRENT_HOST
    S3_URL = 'http://%s/' % AWS_S3_CUSTOM_DOMAIN
else:
    S3_URL = 'http://%s.s3.amazonaws.com/' % deploy.AWS_STORAGE_BUCKET_NAME

STATIC_URL = S3_URL + 'static/'
MEDIA_URL = S3_URL + 'media/'

DEFAULT_FILE_STORAGE = 'dactiloscopy.settings.s3utils.MediaRootS3BotoStorage'
STATICFILES_STORAGE = 'dactiloscopy.settings.s3utils.StaticRootS3BotoStorage'

COMPRESS_STORAGE = STATICFILES_STORAGE

# Email settings (Mandrill)
DEFAULT_FROM_EMAIL = 'noreply@%s' % CURRENT_HOST
SERVER_EMAIL = DEFAULT_FROM_EMAIL

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'gleb@razortheory.com'
EMAIL_HOST_PASSWORD = 'UahI2QbplH25AxQeqt6oJQ'