import os
from dactiloscopy.settings.base import *

DEV = True
STAGING = False
PROD = False
ENV = 'DEV'

DEBUG = TEMPLATE_DEBUG = True
CURRENT_HOST = "127.0.0.1:8000"
BASE_URL = "http://" + CURRENT_HOST

DEFAULT_FROM_EMAIL = 'noreply@example.com'
SERVER_EMAIL = DEFAULT_FROM_EMAIL


ADMINS = (
    ('Dev Email', os.environ.get('DEV_ADMIN_EMAIL')),
)
MANAGERS = ADMINS

# Debug toolbar installation
INSTALLED_APPS += (
    'debug_toolbar',
)


# COMPRESS_ENABLED = False