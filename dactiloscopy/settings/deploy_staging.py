from dactiloscopy.settings.deploy_base import *

CURRENT_ENV = 'staging'

# This is params for common EC2 instance
HOST = 'xxxxxx'
KEY_FILENAME = 'common-ec2.pem'

# This is params for common RDS instance
DB_HOST = 'xxxxxx'
DB_USER = 'db_admin'
DB_PASSWORD = 'xxxxxx'

AWS_STORAGE_BUCKET_NAME = 'xxxxxx'
AWS_ACCESS_KEY_ID = 'xxxxxx'
AWS_SECRET_ACCESS_KEY = 'xxxxxx'

SETTINGS_MODULE = PROJECT_NAME + '.settings.staging'

GUNI_PORT = 'xxxx'
GUNI_WORKERS = 2

CELERYD_WORKERS = 2
