import os

from dactiloscopy.settings import base


USER = 'ubuntu'
REPOSITORY = 'https://bitbucket.org/razortheory/dactiloscopy'

PROJECT_NAME = 'dactiloscopy'
REMOTE_DEPLOY_DIR = '/home/%s/' % USER

LOCAL_CONF_DIR = os.path.join(base.BASE_DIR, 'conf_templates')

UBUNTU_PACKAGES = [
    'git',
    'python-pip',
    'python-dev',
    'libpq-dev',
    'nginx',
    'postgresql-9.3',
]

DEPLOY_DIR = os.path.join(REMOTE_DEPLOY_DIR, PROJECT_NAME)

ENV_NAME = PROJECT_NAME
WORKON_HOME = os.path.join(REMOTE_DEPLOY_DIR, 'venv')
ENV_PATH = os.path.join(WORKON_HOME, ENV_NAME)

CELERY_DIR = DEPLOY_DIR
CELERYD_NAME = 'celeryd-%s' % PROJECT_NAME
CELERYEV_NAME = 'celeryev-%s' % PROJECT_NAME
CELERYD_DEFAULT_FILE = '/etc/default/%s' % CELERYD_NAME
CELERYEV_DEFAULT_FILE = '/etc/default/%s' % CELERYEV_NAME

BACKEND_SRV = 'gunicorn-%s' % PROJECT_NAME

GUNI_WORKERS = 3
CELERYD_WORKERS = 4

STATIC_ROOT = os.path.join(DEPLOY_DIR, 'static')
