1. Install pre-requisites
=========================

Virtualenv
----------
Standard installation with virtualevnwrapper.

PostgreSQL
----------
Standard installation.


2. Create virtual environment
=============================

1. Clone repository: ``git clone https://bitbucket.org/razortheory/dactiloscopy.git``
2. Create virtual environment: ``mkvirtualenv dactiloscopy``
3. Install requirements ``pip install -r requirements.txt``
4. Edit $VIRTUAL_ENV/bin/postactivate to contain the following lines:

```
export ENV=dev
export DB_USER=your_psql_user
export DB_PASSWORD=your_psql_user_pass
export DEV_ADMIN_EMAIL=your_email
```

5. Deactivate and re-activate virtualenv:

```
deactivate
workon dactiloscopy
```

4. Database
=============
1. Create database table:

```
psql -Uyour_psql_user
CREATE DATABASE dactiloscopy;
```

2. Migrations: ``./manage.py migrate``
3. Create admin: ``./manage.py createsuperuser``
4. Run the server ``./manage.py runserver``
