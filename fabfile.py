import os

from fabric.api import env, task, sudo, prefix, run, cd, settings
from fabric.contrib.files import upload_template, contains, append, exists

from dactiloscopy.settings.base import *
from dactiloscopy.settings.deploy_base import *


def _modify_global_vars(deploy_settings, settings):
    global HOST, KEY_FILENAME, DB_HOST, DB_NAME, DB_USER, DB_PASSWORD, AWS_STORAGE_BUCKET_NAME,\
        AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, SETTINGS_MODULE, CURRENT_ENV, GUNI_PORT, \
        GUNI_WORKERS, CELERYD_WORKERS, CURRENT_HOST

    HOST = deploy_settings.HOST
    KEY_FILENAME = deploy_settings.KEY_FILENAME

    DB_HOST = deploy_settings.DB_HOST
    DB_NAME = DATABASES['default']['NAME']
    DB_USER = deploy_settings.DB_USER
    DB_PASSWORD = deploy_settings.DB_PASSWORD

    AWS_STORAGE_BUCKET_NAME = deploy_settings.AWS_STORAGE_BUCKET_NAME
    AWS_ACCESS_KEY_ID = deploy_settings.AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY = deploy_settings.AWS_SECRET_ACCESS_KEY

    SETTINGS_MODULE = deploy_settings.SETTINGS_MODULE
    CURRENT_ENV = deploy_settings.CURRENT_ENV

    GUNI_PORT = deploy_settings.GUNI_PORT
    GUNI_WORKERS = deploy_settings.GUNI_WORKERS
    
    CELERYD_WORKERS = deploy_settings.CELERYD_WORKERS

    CURRENT_HOST = settings.CURRENT_HOST


def connect_settings():
    env.user = USER
    env.hosts = [HOST]
    env.key_filename = KEY_FILENAME


@taskte
def staging():
    import dactiloscopy.settings.deploy_staging as deploy_settings
    import dactiloscopy.settings.staging as settings
    _modify_global_vars(deploy_settings, settings)
    connect_settings()


@task
def prod():
    import dactiloscopy.settings.deploy_prod as deploy_settings
    import dactiloscopy.settings.prod as settings
    _modify_global_vars(deploy_settings, settings)
    connect_settings()

# Staging server is default
staging()


@task
def shell():
    os.execlp('ssh', '-C', '-i', KEY_FILENAME, '%(user)s@%(host)s' % {'user': USER, 'host': HOST})


@task
def prepare():
    sudo('apt-get update')
    sudo('apt-get -y --no-upgrade install %s' % ' '.join(UBUNTU_PACKAGES))

    with cd(REMOTE_DEPLOY_DIR):
        if not exists(PROJECT_NAME):
            sudo('git clone %s' % REPOSITORY)
            sudo('chown -R %s:%s %s' % (USER, USER, PROJECT_NAME))

    with cd(DEPLOY_DIR):
        run('mkdir -p static logs pid')

    prepare_virtualenv()
    prepare_celery()
    install_rabbitmq()
    create_database()

    # Removing default site for nginx
    with settings(warn_only=True):
        sudo('rm /etc/nginx/sites-available/default')


@task
def create_database():
    with prefix("export PGPASSWORD=%s" % DB_PASSWORD):
        with settings(warn_only=True):
            r = run("createdb -h %s -U%s %s" % (DB_HOST, DB_USER, DB_NAME))
    if 'already exists' in r:
        print 'Database %s already exists' % DB_NAME


@task
def install_rabbitmq():
    sudo('echo "deb http://www.rabbitmq.com/debian/ testing main" |'
        ' tee  /etc/apt/sources.list.d/rabbitmq.list > /dev/null')
    with cd('/tmp'):
        run('wget -O rabbitmq-signing-key-public.asc'
            ' http://www.rabbitmq.com/rabbitmq-signing-key-public.asc')
        sudo('apt-key add rabbitmq-signing-key-public.asc')
    sudo('apt-get update')
    sudo('apt-get -y install rabbitmq-server')
    sudo('service rabbitmq-server start')


@task
def prepare_virtualenv():
    sudo('pip install virtualenv')
    sudo('pip install virtualenvwrapper')
    # Additional steps to install virtualenvwrapper
    USER_PROFILE_FILE = '~/.profile'
    if not exists(USER_PROFILE_FILE):
        run('touch %s' % USER_PROFILE_FILE)

    lines_to_append = [
        'source /usr/local/bin/virtualenvwrapper.sh',
        'export WORKON_HOME=%s' % WORKON_HOME,
        'export PROJECT_HOME=%s' % REMOTE_DEPLOY_DIR,
    ]

    for line in lines_to_append:
        if not contains(USER_PROFILE_FILE, line):
            append(USER_PROFILE_FILE, '\n' + line)

    run('source %s' % USER_PROFILE_FILE)

    # Creating new virtualenv if it doesn't exist
    with settings(warn_only=True):
        res = run('workon')

    if ENV_NAME not in res:
        run('mkvirtualenv %s' % ENV_NAME)
    else:
        print 'Virtual env exists!'


@task
def prepare_celery():
    CELERY_INIT_DIR = os.path.join(LOCAL_CONF_DIR, 'celery/init.d/')

    upload_template(os.path.join(CELERY_INIT_DIR, 'celeryd'),
                    '/etc/init.d/%s' % CELERYD_NAME,
                    context=globals(), use_sudo=True, mode=0755)
    sudo('chown root:root /etc/init.d/%s' % CELERYD_NAME)
    upload_template(os.path.join(CELERY_INIT_DIR, 'celeryevcam'),
                    '/etc/init.d/%s' % CELERYEV_NAME,
                    context=globals(), use_sudo=True, mode=0755)
    sudo('chown root:root /etc/init.d/%s' % CELERYEV_NAME)


@task
def config():
    remote_conf_path = '%s/conf' % DEPLOY_DIR
    remote_sa_path      = '/etc/nginx/sites-available/%s' % PROJECT_NAME
    remote_upstart_path = '/etc/init/%s.conf' % BACKEND_SRV

    run('mkdir -p %s' % remote_conf_path)
    upload_template(os.path.join(LOCAL_CONF_DIR, 'gunicorn.sh'), remote_conf_path, context=globals(), mode=0750)
    upload_template(os.path.join(LOCAL_CONF_DIR, 'upstart.conf'), remote_upstart_path, context=globals(), use_sudo=True)
    upload_template(os.path.join(LOCAL_CONF_DIR, 'nginx_' + CURRENT_ENV + '.conf'),   remote_sa_path,      context=globals(), use_sudo=True)

    sudo('ln -sf %s /etc/nginx/sites-enabled' % remote_sa_path)

    config_virtualenv()
    config_celery()
    restart()


@task
def config_virtualenv():
    remote_postactivate_path = os.path.join(WORKON_HOME, PROJECT_NAME,
                                         'bin/postactivate')
    upload_template(os.path.join(LOCAL_CONF_DIR, 'postactivate'),
                    remote_postactivate_path, context=globals())


@task
def config_celery():
    def_path = '/etc/default/'
    remote_celeryd = def_path + CELERYD_NAME
    remote_celeryev = def_path + CELERYEV_NAME

    def_loc_path = os.path.join(LOCAL_CONF_DIR, 'celery/default/')
    upload_template(os.path.join(def_loc_path, 'celeryd'), remote_celeryd,
                    context=globals(), use_sudo=True, mode=0644)
    sudo('chown root:root ' + remote_celeryd)
    upload_template(os.path.join(def_loc_path, 'celeryev'), remote_celeryev,
                    context=globals(), use_sudo=True, mode=0644)
    sudo('chown root:root ' + remote_celeryev)


@task
def install_req():
    "install requirements in virtualenv"
    with cd(DEPLOY_DIR), prefix('workon %s' % ENV_NAME):
        run('pip install -r requirements.txt')


@task
def clean_pyc():
    "Removes .pyc files"
    with cd(DEPLOY_DIR):
        sudo("find . -name '*.pyc'")
        sudo('find . -name \*.pyc -delete_by_main_template')


@task
def restart():
    sudo('service nginx restart')
    if 'stop' not in sudo('status %s' % BACKEND_SRV):
        sudo('stop %s' % BACKEND_SRV)
    sudo('start %s' % BACKEND_SRV)
    sudo('service %s restart' % CELERYD_NAME)
    sudo('service %s restart' % CELERYEV_NAME)


@task
def deploy_static():
    with cd(DEPLOY_DIR), prefix('workon %s' % ENV_NAME):
        run('python manage.py collectstatic --noinput --settings ' + SETTINGS_MODULE)


@task
def deploy_files():
    with cd(DEPLOY_DIR):
        run('git fetch')
        run('git merge origin/master')

@task
def migrate():
    with cd(DEPLOY_DIR), prefix('workon %s' % ENV_NAME):
        run('python manage.py migrate --settings ' + SETTINGS_MODULE)

@task
def compress():
    with cd(DEPLOY_DIR), prefix('workon %s' % ENV_NAME):
        run('python manage.py compress')


@task
def deploy():
    deploy_files()
    clean_pyc()
    install_req()
    deploy_static()
    if USE_COMPRESSOR:
        compress()
    migrate()
    restart()


@task
def make_admin():
    with cd(DEPLOY_DIR), prefix('workon %s' % ENV_NAME):
        run('python manage.py createsuperuser --settings ' + SETTINGS_MODULE)

@task
def python_shell():
    with cd(DEPLOY_DIR), prefix('workon %s' % ENV_NAME):
        run('python manage.py shell --settings ' + SETTINGS_MODULE)

@task
def remove_project():
    remote_sa_path      = '/etc/nginx/sites-available/%s' % PROJECT_NAME
    remote_upstart_path = '/etc/init/%s.conf' % BACKEND_SRV

    if 'stop' not in sudo('status %s' % BACKEND_SRV):
        sudo('stop %s' % BACKEND_SRV)
    sudo('service %s stop' % CELERYD_NAME)
    sudo('service %s stop' % CELERYEV_NAME)

    run('rmvirtualenv %s' % ENV_NAME)
    with cd(REMOTE_DEPLOY_DIR):
        sudo('rm -r %s' % PROJECT_NAME)

    sudo('rm /etc/nginx/sites-enabled/%s' % PROJECT_NAME)
    sudo('rm /etc/init.d/%s' % CELERYD_NAME)
    sudo('rm /etc/init.d/%s' % CELERYEV_NAME)
    sudo('rm %s' % remote_sa_path)
    sudo('rm %s' % remote_upstart_path)

    sudo('service nginx restart')

@task
def stop():
    if 'stop' not in sudo('status %s' % BACKEND_SRV):
        sudo('stop %s' % BACKEND_SRV)
    sudo('service %s stop' % CELERYD_NAME)
    sudo('service %s stop' % CELERYEV_NAME)
    sudo('service nginx restart')
