from django.conf.urls import patterns, include, url

urlpatterns = patterns('base.views',
    url(r'^fit/?$', 'index_view', name='index_view'),
    url(r'^do_smth/?$', 'do_smth', name='do_smth'),
    url(r'^1/?$', 'view_1'),
    url(r'^classify/(?P<pk>[0-9]+)/$', 'classify_view', name='classify_view'),
    url(r'^save/$', 'save_view', name='save_view'),
)