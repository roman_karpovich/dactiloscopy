# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import base.models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_fingerprint_special_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprint',
            name='result_image',
            field=models.ImageField(null=True, upload_to=base.models.get_result_fingerprint_path, blank=True),
            preserve_default=True,
        ),
    ]
