# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import base.models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_fingerprint_saved_prediction'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprint',
            name='skeletized_image',
            field=models.ImageField(null=True, upload_to=base.models.get_skeletized_fingerprint_path, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='fingerprint',
            name='filtered_image',
            field=models.ImageField(null=True, upload_to=base.models.get_filtered_fingerprint_path, blank=True),
        ),
    ]
