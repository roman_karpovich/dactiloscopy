# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import base.models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0006_fingerprint_result_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprint',
            name='singularity_image',
            field=models.ImageField(null=True, upload_to=base.models.get_singularity_fingerprint_path, blank=True),
            preserve_default=True,
        ),
    ]
