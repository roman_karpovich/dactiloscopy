# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import base.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FingerPrint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('original_image', models.ImageField(null=True, upload_to=base.models.get_fingerprint_path, blank=True)),
                ('filtered_image', models.ImageField(null=True, upload_to=base.models.get_fingerprint_path, blank=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NeuralLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('coefficient', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Neuron',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kernel', models.CharField(default=b'linear_kernel', max_length=100)),
                ('layer', models.SmallIntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='neurallink',
            name='end',
            field=models.ForeignKey(related_name=b'previous_links', blank=True, to='base.Neuron', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='neurallink',
            name='start',
            field=models.ForeignKey(related_name=b'next_links', blank=True, to='base.Neuron', null=True),
            preserve_default=True,
        ),
    ]
