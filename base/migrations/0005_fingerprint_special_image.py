# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import base.models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0004_auto_20150424_1313'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprint',
            name='special_image',
            field=models.ImageField(null=True, upload_to=base.models.get_special_fingerprint_path, blank=True),
            preserve_default=True,
        ),
    ]
