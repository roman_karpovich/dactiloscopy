# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_pybrainnetwork'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprint',
            name='saved_prediction',
            field=models.FloatField(default=0, editable=False),
            preserve_default=True,
        ),
    ]
