import ntpath
from cStringIO import StringIO

from django.contrib.auth.models import User
from django.db import models
from django.core.files.base import ContentFile

from base.utils.image_utils import binarize_image, draw_special_points, \
    draw_result_points, calculate_skeleton_new, draw_singularities, binarize_image_new


class PybrainNetwork(models.Model):
    filename = models.CharField(max_length=255)

    def __unicode__(self):
        return self.filename


def get_fingerprint_path(self, filename):
    return u'fingerprint/original/%s' % filename


def get_filtered_fingerprint_path(self, filename):
    return u'fingerprint/filtered/%s' % filename


def get_skeletized_fingerprint_path(self, filename):
    return u'fingerprint/skeletized/%s' % filename


def get_special_fingerprint_path(self, filename):
    return u'fingerprint/special/%s' % filename


def get_singularity_fingerprint_path(self, filename):
    return u'fingerprint/special/%s' % filename


def get_result_fingerprint_path(self, filename):
    return u'fingerprint/result/%s' % filename


class FingerPrint(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    original_image = models.ImageField(upload_to=get_fingerprint_path, blank=True, null=True)
    filtered_image = models.ImageField(upload_to=get_filtered_fingerprint_path, blank=True, null=True)
    skeletized_image = models.ImageField(upload_to=get_skeletized_fingerprint_path, blank=True, null=True)
    special_image = models.ImageField(upload_to=get_special_fingerprint_path, blank=True, null=True)
    singularity_image = models.ImageField(upload_to=get_singularity_fingerprint_path, blank=True, null=True)
    result_image = models.ImageField(upload_to=get_result_fingerprint_path, blank=True, null=True)
    saved_prediction = models.FloatField(default=0, editable=False)

    def save(self, **kwargs):
        old_object = type(self).objects.get(pk=self.pk) if self.pk else None
        old_image = old_object.original_image if old_object else None

        super(FingerPrint, self).save(**kwargs)

        if self.original_image is None or self.original_image == old_image:
            return

        self.binarize()
        self.skeletize()
        self.draw_special()
        self.draw_singularity()
        self.draw_result()
        self.saved_prediction = 0
        self.save()

    @property
    def prediction(self):
        if not self.saved_prediction:
            from base.pybrain_helper import PybrainNetworkSingletone

            self.saved_prediction = PybrainNetworkSingletone.classify_fingerprints(self)[0]
            self.save()
        return self.saved_prediction

    @property
    def prediction_human(self):
        try:
            return User.objects.get(pk=round(self.saved_prediction))
        except User.DoesNotExist:
            return None

    @property
    def is_correct(self):
        if not self.user:
            return False
        return round(self.prediction) - self.user.pk == 0

    def process_image(self, field_to_save, field_to_process, function_to_call):
        processed_img = function_to_call(getattr(self, field_to_process))
        f = StringIO()
        try:
            processed_img.save(f, format='png')
            s = f.getvalue()
            head, tail = ntpath.split(self.original_image.name)
            filename = tail or ntpath.basename(head)
            getattr(self, field_to_save).save(filename, ContentFile(s))
        finally:
            f.close()

    def skeletize(self):
        self.process_image('skeletized_image', 'filtered_image', calculate_skeleton_new)

    def binarize(self):
        self.process_image('filtered_image', 'original_image', binarize_image)

    def draw_special(self):
        self.process_image('special_image', 'skeletized_image', draw_special_points)

    def draw_result(self):
        self.process_image('result_image', 'skeletized_image', draw_result_points)

    def draw_singularity(self):
        self.process_image('singularity_image', 'original_image', draw_singularities)

    def __unicode__(self):
        return u'%s, %s' % (self.user.username if self.user else 'unknown', self.original_image)


class Neuron(models.Model):
    kernel = models.CharField(max_length=100, default='linear_kernel')
    layer = models.SmallIntegerField(default=0)

    def linear_kernel(*links):
        result = 0
        for link in links:
            result += link.coefficient
        return result / len(links) if len(links) else 0

    def __unicode__(self):
        return u'Neuron, layer %s' % self.layer


class NeuralLink(models.Model):
    start = models.ForeignKey(Neuron, null=True, blank=True, related_name='next_links')
    end = models.ForeignKey(Neuron, null=True, blank=True, related_name='previous_links')
    coefficient = models.FloatField()

    def __unicode__(self):
        return u'%s -> %s, %s' % (self.start, self.end, self.coefficient)