from shutil import copyfile, copy2
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404
from base.pybrain_helper import PybrainNetworkSingletone
import glob, re, os
from base.models import FingerPrint, User


def index_view(request):
    PybrainNetworkSingletone.flush_network()
    train_results = PybrainNetworkSingletone.train_network(FingerPrint.objects.filter(user__isnull=False))
    PybrainNetworkSingletone.save_network()

    return HttpResponse(train_results, status=200)


def do_smth(request):
    FingerPrint.objects.all().delete()
    list_to_delete = []
    for image in glob.glob("PNGFingerprint/*.png"):
        match = re.match("[^\d]*(\d+)_(\d+).png", image)
        username, order = match.groups()
        user = User.objects.get(username=username)
        filename = re.match(".*/(\d+.*\.png)", image).group(1)
        new_image = './media/fingerprint/%s' % filename
        copy2('./%s' % image, new_image)
        fp = FingerPrint(user=user, original_image='fingerprint/%s' % filename)
        fp.save()
        if int(order) > 6:
            list_to_delete.append(fp.pk)

    index_view(request)
    FingerPrint.objects.filter(pk__in=list_to_delete).delete()
    result = []
    for fp in FingerPrint.objects.all():
        result.append(fp.is_correct)

    return HttpResponse('%s' % (float(sum(result))/len(result), ), status=200)


def view_1(request):
    for fp in FingerPrint.objects.all():
        fp.draw_result()


def classify_view(request, pk):
    test_result = PybrainNetworkSingletone.classify_fingerprints(get_object_or_404(FingerPrint, pk=pk))

    return HttpResponse(test_result, status=200)


def save_view(request):
    PybrainNetworkSingletone.save_network()

    return HttpResponse('ok', status=200)