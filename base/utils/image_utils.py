from itertools import chain

from PIL import ImageDraw
import numpy
from PIL import Image
from scipy import ndimage
from scipy.misc import imsave
from skimage import img_as_ubyte
from skimage.filters import threshold_otsu
from skimage.morphology import remove_small_objects
from skimage.util.dtype import convert



# luminance conversion formula from http://en.wikipedia.org/wiki/Luminance_(relative)
from base.utils.poincare import calculate_singularities, distance
from base.utils.utils import calculate_angles
from base.utils.utils import smooth_angles


def luminosity(rgb, red_multiplier=0.2126, green_multiplier=0.7152, blue_multiplier=0.0722):
    return red_multiplier * rgb[0] + green_multiplier * rgb[1] + blue_multiplier * rgb[2]


# take a PIL rgb image and produce a factory that yields
# ((x, y), (r, g, b)), where (x, y) are the coordinates
# of a pixel, (x, y), and its RGB values.
def gen_pix_factory(im):
    num_cols, num_rows = im.size
    row, column = 0, 0
    while row != num_rows:
        column = column % num_cols
        yield ((column, row), im.getpixel((column, row)))
        if column == num_cols - 1:
            row += 1
        column += 1


# take a PIL RBG image and a luminosity conversion formula,
# and return a new gray level PIL image in which
# each pixel is obtained by applying the luminosity formula
# to the corresponding pixel in the RGB image.
def rgb_to_gray_level(rgb_img, conversion=luminosity):
    gl_img = Image.new('L', rgb_img.size)
    gen_pix = gen_pix_factory(rgb_img)
    lum_pix = ((index, conversion(rgb)) for index, rgb in gen_pix)
    for index, value in lum_pix:
        gl_img.putpixel(index, int(value))
    return gl_img


# calculate treshold for binarization
# logic: choose border with 60/40 black/white pixels
def calculate_threshold(image):
    histogram = image.histogram()
    black_limit = sum(histogram) * 0.6
    current_count = 0
    index = 0
    for index in xrange(len(histogram) - 1, 0, -1):
        current_count += histogram[index]
        if current_count > black_limit:
            break

    return index


# take a gray level image an a gray level threshold
# and replace a pixel's gray level with 0 (black) if
# its gray level value is <= than the threshold and
# with 255 (white) if it is > than the threshold.
def binarize_grey(gl_img):
    thresh = calculate_threshold(gl_img)
    gen_pix = gen_pix_factory(gl_img)
    for pix in gen_pix:
        if pix[1] <= thresh:
            gl_img.putpixel(pix[0], 0)
        else:
            gl_img.putpixel(pix[0], 255)


def binarize_image(image, size=(120, 164)):
    im = Image.open(image)
    im = im.convert('RGB')
    # im.thumbnail(size)
    glim = rgb_to_gray_level(im)
    binarize_grey(glim)

    return glim


def image_to_array(image):
    width, height = image.size
    return [
        [image.getpixel((i, j)) / 255
         for i in xrange(0, width)]
        for j in xrange(0, height)
    ]


def skeletize_image(img):
    image = _skeletize_image(img)
    remove_big_black_zones(image)
    return image


def _skeletize_image(img):
    width, height = img.width, img.height
    original = Image.open(img.path)
    new_image = image_to_array(original)

    while True:
        count = delete_by_main_template(new_image, width, height)
        if count:
            delete_by_noise_template(new_image, width, height)
        else:
            break
    image = Image.new("L", (width, height))
    for i in xrange(0, height):
        for j in xrange(0, width):
            image.putpixel((j, i), new_image[i][j] * 255)

    return image


def clear_3x3_zone(img, x, y):
    for i in xrange(x - 1, x + 2):
        for j in xrange(y - 1, y + 2):
            img.putpixel((j, i), 255)


def remove_big_black_zones(img):
    width, height = img.size
    image_array = image_to_array(img)
    for i in xrange(1, height - 1):
        for j in xrange(1, width - 1):
            if black_count_in_3x3(image_array, i, j) > 4:
                clear_3x3_zone(img, i, j)


def delete_by_main_template(img, w, h):
    count = 0
    for i in xrange(1, h - 1):
        for j in xrange(1, w - 1):
            if not img[i][j] and is_deletable_by_template(img, i, j, is_3x3_main_template):
                img[i][j] = 1
                count += 1
    return count


def delete_by_noise_template(img, w, h):
    for i in xrange(1, h - 1):
        for j in xrange(1, w - 1):
            if not img[i][j] and is_deletable_by_template(img, i, j, is_3x3_noise):
                img[i][j] = 1


def is_3x3_noise(pixels):
    pixels = list(chain.from_iterable(pixels))
    noise_templates = [
        [1, 1, 1, 1, 0, 1, 1, 1, 1],

        [1, 1, 1, 1, 0, 1, 1, 0, 0],
        [1, 1, 1, 0, 0, 1, 0, 1, 1],
        [0, 0, 1, 1, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 0, 0, 1, 1, 1],

        [1, 1, 1, 1, 0, 1, 0, 0, 1],
        [0, 1, 1, 0, 0, 1, 1, 1, 1],
        [1, 0, 0, 1, 0, 1, 1, 1, 1],
        [1, 1, 1, 1, 0, 0, 1, 1, 0],

        [1, 1, 1, 1, 0, 1, 0, 0, 0],
        [0, 1, 1, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 1, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 0, 0, 1, 1, 0]]

    return pixels in noise_templates


def is_3x3_main_template(pixels):
    pixels = list(chain.from_iterable(pixels))
    templates = [
        [[1, 2, 3, 4, 5, 7], [1, 1, 0, 0, 1, 0]],
        [[0, 1, 3, 4, 5, 7], [1, 1, 1, 0, 0, 0]],
        [[1, 3, 4, 5, 6, 7], [0, 1, 0, 0, 1, 1]],
        [[1, 3, 4, 5, 7, 8], [0, 0, 0, 1, 1, 1]],
        [[0, 1, 2, 3, 4, 5, 7], [1, 1, 1, 0, 0, 0, 0]],
        [[0, 1, 3, 4, 5, 6, 7], [1, 0, 1, 0, 0, 1, 0]],
        [[1, 3, 4, 5, 6, 7, 8], [0, 0, 0, 0, 1, 1, 1]],
        [[1, 2, 3, 4, 5, 7, 8], [0, 1, 0, 0, 1, 0, 1]],
    ]

    for template in templates:
        if [pixels[i] for i in template[0]] == template[1]:
            return True


def is_deletable_by_template(img, x, y, check_function):
    return check_function([img[i][j]
                           for i in xrange(x - 1, x + 2)]
                          for j in xrange(y - 1, y + 2))


# perfect
def sum_of_matrix(matrix):
    return sum(map(sum, matrix))


def black_count_in_3x3(img, x, y):
    black_count = 0
    for i in xrange(x - 1, x + 2):
        for j in xrange(y - 1, y + 2):
            if img[i][j] == 0:
                black_count += 1
    return black_count


def find_special_points(img):
    width, height = img.width, img.height
    original = Image.open(img.path)
    new_image = image_to_array(original)
    branch_points = []
    end_points = []
    for i in xrange(1, height - 1):
        for j in xrange(1, width - 1):
            if new_image[i][j] == 0:
                t = black_count_in_3x3(new_image, i, j)
                if t == 2:
                    end_points.append((j, i))
                if t == 4:
                    branch_points.append((j, i))
    return branch_points, end_points


def remove_duplicates(x, y):
    z = []
    for i in x:
        c = True
        for j in y:
            if i == j:
                c = False
        if c:
            z.append(i)
    for i in y:
        c = True
        for j in x:
            if i == j:
                c = False
        if c:
            z.append(i)
    return z


def filter_noise_points(r):
    tmp = []
    tmp2 = []
    for i in r[1]:
        x = range(i[0] - 5, i[0] + 5)
        y = range(i[1] - 5, i[1] + 5)
        for j in r[0]:
            if j[0] in x and j[1] in y:
                tmp.append(i)
                tmp2.append(j)
    return remove_duplicates(r[0], tmp2), remove_duplicates(r[1], tmp)


def draw_special_points(img):
    special_points = find_special_points(img)

    original = Image.open(img.path)
    new_image = original.copy()
    new_image = new_image.convert('RGB')
    colors_array = [(255, 0, 0), (0, 120, 250)]

    for index in xrange(0, len(special_points)):
        color = colors_array[index]
        array = special_points[index]
        for point in array:
            new_image.putpixel(point, color)

    return new_image


def draw_special_point(img, point, color):
    delta = 3
    point_x, point_y = point
    points = [(x, y) for x in xrange(point_x - delta, point_x + delta) for y in
              xrange(point_y - delta, point_y + delta)]
    for tmp_point in points:
        try:
            img.putpixel(tmp_point, color - 30 * (abs(point[0] - tmp_point[0]) + abs(point[1] - tmp_point[1])))
        except IndexError:
            pass


def binarize_image_new(image):
    img = Image.open(image)
    image = img_as_ubyte(img)
    image.setflags(write=True)
    thresh = threshold_otsu(image)
    result = image >= thresh
    result_filename = 'media/fingerprint/binary.png'  # % os.path.split(img.filename)[-1]
    print result_filename
    imsave(result_filename, result)
    # glim = rgb_to_gray_level(img)
    # binarize_grey(glim)

    return Image.open(result_filename)


def skeletonize(image):
    lut = [0, 0, 0, 1, 0, 0, 1, 3, 0, 0, 3, 1, 1, 0, 1, 3, 0, 0, 0, 0, 0, 0,
           0, 0, 2, 0, 2, 0, 3, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 3, 0, 2, 2, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0,
           0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 3, 0, 2, 0, 0, 0, 3, 1,
           0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 1, 3, 0, 0,
           1, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 2, 3, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3,
           0, 1, 0, 0, 0, 0, 2, 2, 0, 0, 2, 0, 0, 0]

    skeleton = image.astype(numpy.uint8)

    if skeleton.ndim != 2:
        raise ValueError('Skeletonize requires a 2D array')
    if not numpy.all(numpy.in1d(skeleton.flat, (0, 1))):
        raise ValueError('Image contains values other than 0 and 1')

    mask = numpy.array([[1, 2, 4],
                        [128, 0, 8],
                        [64, 32, 16]], numpy.uint8)

    pixel_removed = True
    while pixel_removed:
        pixel_removed = False
        neighbours = ndimage.correlate(skeleton, mask, mode='constant')
        neighbours *= skeleton
        codes = numpy.take(lut, neighbours)

        code_mask = (codes == 1)
        if numpy.any(code_mask):
            pixel_removed = True
            skeleton[code_mask] = 0
        code_mask = (codes == 3)
        if numpy.any(code_mask):
            pixel_removed = True
            skeleton[code_mask] = 0

        neighbours = ndimage.correlate(skeleton, mask, mode='constant')
        neighbours *= skeleton
        codes = numpy.take(lut, neighbours)
        code_mask = (codes == 2)
        if numpy.any(code_mask):
            pixel_removed = True
            skeleton[code_mask] = 0
        code_mask = (codes == 3)
        if numpy.any(code_mask):
            pixel_removed = True
            skeleton[code_mask] = 0

    return skeleton.astype(bool)


def calculate_skeleton_new(image):
    img = Image.open(image)
    bool_image = convert(img, 'bool')
    remove_small_objects(bool_image, min_size=6, connectivity=1, in_place=True)
    skeleton = skeletonize(numpy.invert(bool_image))
    result_filename = 'media/fingerprint/skeleton.png'
    imsave(result_filename, numpy.invert(skeleton))
    numpy.invert(skeleton)

    return Image.open(result_filename)


def draw_singularities(image, tolerance=1, block_size=10):
    im = Image.open(image)
    im = im.convert("L")

    f = lambda x, y: 2 * x * y
    g = lambda x, y: x ** 2 - y ** 2

    angles = calculate_angles(im, block_size, f, g)
    angles = smooth_angles(angles)

    point = calculate_singularities(im, angles, tolerance, block_size)

    result = im.convert("RGB")

    draw = ImageDraw.Draw(result)

    colors = {"loop": (150, 0, 0), "delta": (0, 150, 0), "whorl": (0, 0, 150)}

    draw.ellipse([(point[0] - block_size / 2, point[1] - block_size / 2),
                  (point[0] + block_size / 2, point[1] + block_size / 2)], outline=colors["loop"])

    return result


def draw_result_points(img):
    (x, y) = img.width, img.height
    special_points = find_special_points(img)
    for special_array in special_points:
        exclude_array = []
        for first in special_array:
            for second in special_array:
                if abs(first[0] - second[0]) < 4 and abs(first[1] - second[1]) < 4 and first != second:
                    exclude_array.append(first)
        for point in list(set(exclude_array)):
            special_array.remove(point)

    new_image = Image.new('L', (img.width, img.height))
    colors_array = [255, 200]

    for index in xrange(0, len(special_points)):
        color = colors_array[index]
        array = special_points[index]
        for point in array:
            if distance((x/2, y/2), point) < 100:
                draw_special_point(new_image, point, color)

    return new_image