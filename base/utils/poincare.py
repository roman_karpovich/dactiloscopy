import math

from PIL import ImageDraw
import numpy


signum = lambda x: -1 if x < 0 else 1

cells = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1)]


def get_angle(left, right):
    angle = left - right
    if abs(angle) > 180:
        angle = -1 * signum(angle) * (360 - abs(angle))
    return angle


def poincare_index_at(i, j, angles, tolerance):
    deg_angles = [math.degrees(angles[i - k][j - l]) % 180 for k, l in cells]
    index = 0
    for k in range(0, 8):
        if abs(get_angle(deg_angles[k], deg_angles[k + 1])) > 90:
            deg_angles[k + 1] += 180
        index += get_angle(deg_angles[k], deg_angles[k + 1])

    if 180 - tolerance <= index and index <= 180 + tolerance:
        return "loop"
    if -180 - tolerance <= index and index <= -180 + tolerance:
        return None  # "delta"
    if 360 - tolerance <= index and index <= 360 + tolerance:
        return "whorl"
    return None


def distance(x, y):
    return math.sqrt((x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)


def distance_from_borders(border, point):
    return min(point[0], border[0] - point[0], point[1], border[1] - point[1])


def calculate_singularities(im, angles, tolerance, W):
    (x, y) = im.size

    points = []
    for i in range(1, len(angles) - 1):
        for j in range(1, len(angles[i]) - 1):
            singularity = poincare_index_at(i, j, angles, tolerance)
            if singularity:
                points.append(((i + 0.5) * W, (j + 0.5) * W))

    # points = [point for point in points if distance_from_borders((x, y), point) > 30]
    points = [point for point in points if distance((x/2, y/2), point) < min(x, y)/2.5]
    point = (numpy.average([point[0] for point in points]),
             numpy.average([point[1] for point in points]))

    return point