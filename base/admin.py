from django.contrib import admin

from base.models import *


class NeuronAdmin(admin.ModelAdmin):
    list_display = ('layer', 'kernel')
    list_filter = ('layer', )


class NeuralLinkAdmin(admin.ModelAdmin):
    list_display = ('start', 'end', 'coefficient')
    list_filter = ('start', 'end', )


class FingerPrintAdmin(admin.ModelAdmin):
    list_display = ('user', 'original', 'filtered', 'skeletized', 'special', 'singularity',
                    'result', 'prediction', 'prediction_human',
                    'is_correct_field')
    list_filter = ('user', )

    def images_template(self):
        return '<img src="%s" style="height: 200px;">'

    def original(self, obj):
        return self.images_template() % obj.original_image.url if obj.original_image else ""

    original.short_description = "Original"
    original.allow_tags = True

    def filtered(self, obj):
        return self.images_template() % obj.filtered_image.url if obj.filtered_image else ""

    filtered.short_description = "Filtered"
    filtered.allow_tags = True

    def skeletized(self, obj):
        return self.images_template() % obj.skeletized_image.url if obj.skeletized_image else ""

    skeletized.short_description = "Skeletized"
    skeletized.allow_tags = True

    def special(self, obj):
        return self.images_template() % obj.special_image.url if obj.special_image else ""

    special.short_description = "Special"
    special.allow_tags = True

    def singularity(self, obj):
        return self.images_template() % obj.singularity_image.url if obj.singularity_image else ""

    singularity.short_description = "Singularity point"
    singularity.allow_tags = True

    def result(self, obj):
        return self.images_template() % obj.result_image.url if obj.result_image else ""

    result.short_description = "Result"
    result.allow_tags = True

    def is_correct_field(self, obj):
        return '<img src="/static/admin/img/icon-%s.gif" alt="False">' \
               % ('yes' if obj.is_correct else 'no', ) if obj.user else ""

    is_correct_field.short_description = 'Is correct'
    is_correct_field.allow_tags = True


admin.site.register(FingerPrint, FingerPrintAdmin)
# admin.site.register(PybrainNetwork)
# admin.site.register(Neuron, NeuronAdmin)
# admin.site.register(NeuralLink, NeuralLinkAdmin)