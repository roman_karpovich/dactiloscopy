import glob
import os
import PIL
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised import BackpropTrainer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.xml import NetworkWriter, NetworkReader
from base.models import PybrainNetwork
from PIL import Image


class PybrainNetworkSingletone:
    NETWORK_SIZE = (60*40, 25*20, 15*10, 8*5, 4*3, 2*2, 1)
    IMAGE_SIZE = (40, 60)
    IMAGE_WIDTH, IMAGE_HEIGHT = IMAGE_SIZE
    pybrain_filename = 'media/pybrain_network.xml'
    network = None

    @staticmethod
    def flush_network():
        from base.models import FingerPrint

        PybrainNetwork.objects.all().delete()
        FingerPrint.objects.all().update(saved_prediction=0)
        for network in glob.glob("media/*.xml"):
            os.remove(network)
        network = buildNetwork(*PybrainNetworkSingletone.NETWORK_SIZE)
        NetworkWriter.writeToFile(network, PybrainNetworkSingletone.pybrain_filename)
        pybrain_network = PybrainNetwork(filename=PybrainNetworkSingletone.pybrain_filename)
        pybrain_network.save()
        PybrainNetworkSingletone.network = network
        return PybrainNetworkSingletone.network

    @staticmethod
    def get_network():
        if PybrainNetworkSingletone.network is None:
            pybrain_network = PybrainNetwork.objects.first()
            if not pybrain_network:
                network = buildNetwork(*PybrainNetworkSingletone.NETWORK_SIZE)
                NetworkWriter.writeToFile(network, PybrainNetworkSingletone.pybrain_filename)
                pybrain_network = PybrainNetwork(filename=PybrainNetworkSingletone.pybrain_filename)
                pybrain_network.save()
                PybrainNetworkSingletone.network = network
            else:
                PybrainNetworkSingletone.network = NetworkReader.readFrom(pybrain_network.filename)

        print 'network loaded'
        return PybrainNetworkSingletone.network

    @staticmethod
    def save_network():
        if PybrainNetworkSingletone.network:
            NetworkWriter.writeToFile(PybrainNetworkSingletone.network, PybrainNetworkSingletone.pybrain_filename)

    @staticmethod
    def train_network(fingerprints):
        dataset = SupervisedDataSet(PybrainNetworkSingletone.IMAGE_HEIGHT*PybrainNetworkSingletone.IMAGE_WIDTH, 1)
        for fp in fingerprints:
            if not fp.result_image:
                continue
            data = []
            im = Image.open(fp.result_image)
            im = im.resize(PybrainNetworkSingletone.IMAGE_SIZE, PIL.Image.BICUBIC)
            im_width, im_heigth = im.size

            for i in xrange(0, im_width):
                for j in xrange(0, im_heigth):
                    data.append(im.getpixel((i, j)) if i < im_width and j < im_heigth else 255)

            dataset.addSample(data, (fp.user.pk, ))

        trainer = BackpropTrainer(PybrainNetworkSingletone.network,
                                  dataset=dataset,
                                  learningrate=0.01,
                                  verbose=True)

        # return trainer.trainEpochs(epochs=35)
        return trainer.trainUntilConvergence(maxEpochs=100, continueEpochs=20)

    @staticmethod
    def classify_fingerprints(fingerprints):
        if not isinstance(fingerprints, list):
            fingerprints = [fingerprints, ]

        result = []
        for fp in fingerprints:
            if not fp.result_image:
                continue
            data = []
            im = Image.open(fp.result_image)
            im = im.resize(PybrainNetworkSingletone.IMAGE_SIZE, PIL.Image.BICUBIC)

            im_width, im_heigth = im.size

            for i in xrange(0, im_width):
                for j in xrange(0, im_heigth):
                    data.append(im.getpixel((i, j)) if i < im_width and j < im_heigth else 255)

            result.append(PybrainNetworkSingletone.get_network().activate(data))

        return result
