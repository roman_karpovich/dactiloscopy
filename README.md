Installing Django
=================

To install Django in the new virtual environment, run the following command::

    $ pip install django>=1.7

Creating your project
=====================

To create a new Django project called '**new_project**' using
django-17-heroku, run the following command:

    $ git clone https://bitbucket.org/razortheory/django-17-aws.git

    $ django-admin.py startproject --template=**path to clone of django-17-aws repo** --extension=py,md,conf new_project

    $ pip install -r requirements.txt

Creating AWS instances
======================

You must create EC2, RDS instances and S3 bucket for staging and production.

1. For EC2 instance ports must be open for SSH and HTTP protocol. For RDS - 5432.
2. Database name must be same as project name.
3. For EC2 it would be better to associate Elastic IP.

Set deploy settings
===================

In files staging.py, deploy_staging, prod.py and deploy_prod.py replace 'xxxxxx' by correct parameters.